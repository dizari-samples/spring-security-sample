package com.dizari.sample.springsecurity.security.auth.jwt.verifier;

/**
 * BloomFilterTokenVerifier
 *
 * This is dummy class. You should ideally implement your own TokenVerifier to check for revoked tokens.
 *
 * @author garfieldcoke
 * @since 25/06/2017.
 */
public class BloomFilterTokenVerifier implements TokenVerifier {
    @Override
    public boolean verify(String jti) {
        return true;
    }
}
