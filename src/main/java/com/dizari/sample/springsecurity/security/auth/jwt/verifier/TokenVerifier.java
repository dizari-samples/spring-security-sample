package com.dizari.sample.springsecurity.security.auth.jwt.verifier;

/**
 * Description
 *
 * @author garfieldcoke
 * @since 25/06/2017.
 */
public interface TokenVerifier {
    public boolean verify(String jti);
}
