package com.dizari.sample.springsecurity.security.model;

/**
 * @autor garfieldcoke
 * @since 25/06/2017.
 */
public enum Scopes {
    REFRESH_TOKEN;

    public String authority() {
        return "ROLE_" + this.name();
    }
}
