package com.dizari.sample.springsecurity.security.exception;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * @author garfieldcoke
 * @since 24/06/2017.
 */
public class AuthMethodNotSupportedException extends AuthenticationServiceException {

    public AuthMethodNotSupportedException(String msg) {
        super(msg);
    }
}
