package com.dizari.sample.springsecurity.security.token;

/**
 * Created by garfieldcoke on 25/06/2017.
 */
public interface JwtToken {
    String getToken();
}
