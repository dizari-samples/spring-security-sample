package com.dizari.sample.springsecurity.profile.controller;

import com.dizari.sample.springsecurity.security.model.UserContext;
import com.dizari.sample.springsecurity.security.token.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * End-point for retrieving logged-in user details.
 *
 * @author garfieldcoke
 * @since 25/06/2017.
 */
@RestController
public class ProfileController {
    @RequestMapping(value="/api/me", method= RequestMethod.GET)
    public @ResponseBody
    UserContext get(JwtAuthenticationToken token) {
        return (UserContext) token.getPrincipal();
    }
}
