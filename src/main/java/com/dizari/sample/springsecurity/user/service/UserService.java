package com.dizari.sample.springsecurity.user.service;

import com.dizari.sample.springsecurity.user.entity.User;

import java.util.Optional;

/**
 * Created by garfieldcoke on 24/06/2017.
 */
public interface UserService {
    public Optional<User> getByUsername(String username);
}
