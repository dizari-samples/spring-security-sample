package com.dizari.sample.springsecurity.user.service.impl;

import com.dizari.sample.springsecurity.user.entity.User;
import com.dizari.sample.springsecurity.user.repository.UserRepository;
import com.dizari.sample.springsecurity.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by garfieldcoke on 24/06/2017.
 */
@Service
public class DatabaseUserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public DatabaseUserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }
}
