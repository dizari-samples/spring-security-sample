package com.dizari.sample.springsecurity.user.repository;

import com.dizari.sample.springsecurity.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author garfieldcoke
 * @since 24/06/2017.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT u FROM User u LEFT JOIN FETCH u.roles r WHERE u.username=:username")
    public Optional<User> findByUsername(@Param("username") String username);
}
