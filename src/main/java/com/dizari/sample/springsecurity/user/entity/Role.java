package com.dizari.sample.springsecurity.user.entity;

import com.dizari.sample.springsecurity.user.entity.User;

/**
 * Enumerated {@link User} roles.
 *
 * @author garfieldcoke
 * @since 24/06/2017.
 */
public enum Role {
    ADMIN, PREMIUM_MEMBER, MEMBER;

    public String authority() {
        return "ROLE_" + this.name();
    }
}
